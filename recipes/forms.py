from django.forms import ModelForm
from django import forms
from recipes.models import Recipe

class RecipeForm(ModelForm):
    # email_address = forms.EmailField(max_length=300, required=False)

    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
        ]


